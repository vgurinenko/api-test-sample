import requests
import json

def test_grant_subscription():
  url = "https://api.adapty.io/api/v1/sdk/profiles/b6247169-89cb-485a-a30a-7db96e41017d/paid-access-levels/premium/grant/"

  headers = {
    'Content-Type': 'application/json',
    'Authorization': 'Api-Key secret_live_yedBN7XR.HWgQseTFegP0plExWDJiJloL5Ixld46F'
  }

  subscription_length = json.dumps({
    "is_lifetime": False,
    "expires_at": "2023-12-31T23:59:59",
    "duration_days": 10
  })

  expected_status_code = 200

  response = requests.request("POST", url, headers=headers, data=subscription_length)
  assert response.status_code == expected_status_code


def test_grant_subscription_no_auth():
  url = "https://api.adapty.io/api/v1/sdk/profiles/b6247169-89cb-485a-a30a-7db96e41017d/paid-access-levels/premium/grant/"

  headers = {
    'Content-Type': 'application/json',
    'Authorization': 'secret_live_yedBN7XR.HWgQseTFegP0plExWDJiJloL5Ixld46F'
  }

  subscription_length = json.dumps({
    "is_lifetime": False,
    "expires_at": "2023-12-31T23:59:59",
    "duration_days": 10
  })

  expected_status_code = 401

  response = requests.request("POST", url, headers=headers, data=subscription_length)
  assert response.status_code == expected_status_code


def test_grant_subscription_wrong_endpoint():
  url = "https://api.adapty.io/api/v1/sdk/profiles/b6247169-89cb-485a-a30a-7db96e41017d/paid-access-levels/premium/grab/"

  headers = {
    'Content-Type': 'application/json',
    'Authorization': 'secret_live_yedBN7XR.HWgQseTFegP0plExWDJiJloL5Ixld46F'
  }

  subscription_length = json.dumps({
    "is_lifetime": False,
    "expires_at": "2023-12-31T23:59:59",
    "duration_days": 10
  })

  expected_status_code = 404

  response = requests.request("POST", url, headers=headers, data=subscription_length)
  assert response.status_code == expected_status_code

